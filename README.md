# 从 0 开始学 React

## 开始本课程

```bash
cd ~
mkdir -p worksapce/playground/react-redux-course
cd ~/workspace/playground/react-redux-course
git clone git@git.oschina.net:reactor/react-redux-course-lecture-1.git
cd react-redux-course-lecture-1
npm i --registry=https://registry.npm.taobao.org
npm start
```

## 需求

1. 用户打开网页，可以看到有一个导航条；
2. 在导航条的下面，是一个从上往下排列的资产列表；
3. 用户可以选择某一个资产，查看该资产的详细信息；
4. 用户可以选择并申请使用某一个资产；
5. 用户可以查看自己使用过、申请过以及使用中的所有资产列表。

这样的页面，我们可以打开 [`design.html`](./design.html) 文件，

## 官方示例 create-react-app

```bash
npm install -g create-react-app
create-react-app hello-world
cd hello-world
npm start
```

在接下来的代码示例中，我们可以打开 [Babeljs.io](http://babeljs.io/repl/) 实时的查看自己
编码的代码将被如何运行。

## Hello World - JSX

```es6
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

在上面代码中，ReactDOM 将 `<h1>` 这个组件渲染至 `id` 为 `root` 的 `dom` 元素中。通过
`Babel` 编译后，上面的代码最终看起来会是下面这样的：

```javascript
'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : {
  default: obj }; }

_reactDom2.default.render(_react2.default.createElement(
  'h1',
  null,
  'Hello, world!'
), document.getElementById('root'));
```

### 我们知道了什么？

1. 推荐使用 ES6 来写 React 程序；
2. 在浏览器对 ES6 的支持还不完善的情况下，我们需要使用 Babel 工具将 ES6 代码编译成 ES5；
3. Webpack 会帮我们把 ES6 写的代码，通过 Babel 编译后再打包到一个文件里面去；
4. JSX 是好是坏暂且不说，但至少，我不再需要在 HTML 与 JavaScript 里面来回切换了；
5. 一个组件，最终的 `html` 代码只是用来在浏览器展示界面用而已，而它本质上，只是 React 中
   一个对象而已。

## 组件化

将 `<h1>` 标签提取出来，作成一个组件：

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

function component() {
  return <h1>Hello, world!</h1>;
}

ReactDOM.render(
	component,
	document.getElementById('root')
);
```

Babel 编译后的代码如下：

```javascript
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : {
   default: obj }; }

function component() {
  return _react2.default.createElement(
    'h1',
    null,
    'Hello, world!'
  );
}

exports.default = function () {
  _reactDom2.default.render(component, document.getElementById('root'));
};
```

但是，上面的代码并不会有任何展示。

### 为什么？

1. 你需要传给 `ReactDOM.render` 的，不是组件本身，而是组件的实例（Instance）；
2. 为了与原生的 `html` 标签进行区分，任何一个 `React` 组件，都必须首字母大写；

## 把 `html` 模板转成 JSX 代码

这很简单，有了 JSX，我们可以直接在 `javascript` 中写 `HTML`，所以，直接复制`design.html`
中 `body` 元素以内的内容至 `js` 文件即可，点击 [src/design-jsx.js](src/design-jsx.js)
文件，可以看到我们转换后在JSX代码，然后到浏览器中查看。

这个时候， `terminal` 中应该报错了：

```bash
ERROR in ./src/design-jsx.js
Module build failed: SyntaxError: Adjacent JSX elements must be wrapped in an
enclosing tag
```

为什么？

1. 任何一个组件，最终都只允许被包裹在一个元素中，不允许由多个并行的元素展示一个组件；
2. `class` 是 `ES6` 中的关键字，它和 `es6` 中 **类** `class` 冲突，我们需要换成
   `className` 代替，同样的，在写表单的时候，还有一个 `<label for>` 需要换成
	 `htmlFor`；
3. 与 `html` 不同的还有一点，需要特别注意，那就是任何一个组件或者 `html` 元素，都必须显式的
	 关闭，比如 `<img src="path/to/image.png">` 是不被允许的，我们必须要写成 `<img />`。

## 学会划分组件，找到最佳的功能边界

为什么要组件化？这个可以讲个关于小钱的故事：

1. 09年的时候，小钱毕业，加入了某公司入职，主要工作就是维护公司的网站；
2. 他得用PS设计页面，然后用 FW 切图，用 DW 细调，然后上传内容，然后调整，写点飘窗，恩，他成为
   了一个叫作 **美工** 的物种的一员；
3. 再后来，公司业务发展，老板说，我们的用户可以自己在网站里面下定单，于是，小钱学了一门叫作
   `PHP` 的神一样的编程语言，还学会了一个叫作 `MySQL` 的数据库，网站很快也上线了；
4. 又不过多久，发现定单量还不错，就想着再弄得再高大上一些，此时APP也兴起了，老板想着，小钱，
   你给朕整个APP吧，于是……
5. 在小钱的努力下，公司的技术是没有大的问题的，但是就是，人太累，一个人管着所有的技术，于是，小
   钱就跟老板商量，能不能再招一个人进来专门做网站和APP的设计哪？老板看在小钱一直很努力的份上，
   就同意了。
6. 公司继续发展，两个人也不够了，设计说了，老板，能不能再招一个人哪？我专门做APP的设计，新来的
   专门做网站的设计，小钱也说了，老板能不能再给我来三个？一个人专门管后台开发，一个人专门管网站
   的前台的开发，一个人专门管APP的开发，我在把控整个项目，这样的话，每一个就只要专门管好自己的
   事情就成了，也不容易出错，老板很开明的，想想也是；
7. 再往后发展，曾经一个人做的事情，现在在小钱的公司里面，由几十个人的团队做着，先说说这样有什么
   坏处吧，人多，难管哪，技术水平还参差不齐，一个项目下来，小钱也会纠结一下是给谁去做呢？APP里
   面的一个H5页面，你说是该APP的设计师去设计？还是该WEB设计师是去设计？……
8. 关于上面的种种坏处，其实相比于好处来说，他们都不算啥了，每个人各自只负责自己的那一小块，
   如果出错，也只是小错，同时，把一件大的事情拆成很多个小事情，那就可以同步并行进行，而且小事情
   更容易管控嘛。

其实，组件化跟上面这种公司管理与工作分配的发展特别相似，关键点：

组件化的中心思想都是分而治之，目的都是将一个庞大的系统拆分成多个组件；

> 关于组件化与模块化：
>
> 模块化的目的是为了重用，模块化后可以方便重复使用和插拨到不同的平台，不同的业务逻辑过程中。
>
> 组件化的目的是为了解耦，把系统拆分成多个组件，分离组件边界和责任，便于独立升级和维护。
>
> 这个有点像以前我们经常说的库与框架之间的区别

## 尝试划分

### 第一次拆分

1. 导航条： 只负责保证在任何一个页面，都可以方便的跳转至其它主要页面；
2. 主体内容：专门用于展示页面的主要内容，在本次这个项目里面，就是那个列表

结果我们可以看 [src/component-step-1.js](src/component-step-1.js) 文件，

现在我们有了三个组件了：

1. `App` 我们的APP，
2. `Navigator` 导航条
3. `Body` 主内容

### 第二次拆分

主内容现在由两部门组件，一个页面标题，另一个是一个列表，这两个我们可以再次拆分：

1. Body > Header
2. Body > Items

结果我们可以看 [src/component-step-2.js](src/component-step-2.js) 文件，

### React.Component

现在这个时候，看上去似乎已经无法拆分了，但是其实，我们还可以再一次拆分，可以看到，`Items` 组件
其实是一个有规律可询的组件，它里面由多个条目 `item` 组成， 每一个不同的 `item` 中，其结构都
是一样的，只是里面的内容不一样而已，我们可以怎么样做？

很简单，组件除了可以像上面这样的展示外，还可以像 HTML 一样，为它设定属性，方式也跟HTML一样，比
如：

```jsx
<Items className="my-items-class" />
```

但是， `Items` 组件如何获取到 `className` 这个值呢？这个时候，可以引入一个新的概念了，那就是
`React.Component`，这就是我们在写 React 应用的时候，最常见的东西了， **React 组件**。

在前面的所有示例中，我们任何一个组件都只是一个返回一定 HTML结构的方法而已，这在简单的应用中是没
有问题的，但是，再复杂一点的需求就无法满足了，此时我们就需要使用到 `React.Component`。

引入 `Component`，然后将前面组件都改成更合理的方式，比如：

```es6
const Header = () => {
  return (
    <h1 className="ui header">我的资产</h1>
  )
}
```

我们会改成：

```es6
class Header extends Component {
  render() {
    return (
      <h1 className="ui header">我的资产</h1>
    )
  }
}
```

结果我们可以看 [src/component-step-component.js](src/component-step-component.js)
文件，

### 组件的属性

当一个 `class` 继承了 `Component` 之后，它就具有了 `Component` 所提供的所有功能，我们可以
在组件的内部使用 `this.props` 访问到所有组件当前的属性。

```es6

class Items extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    console.log(this.props);

    ...
  }
}

...


class Body extends Component {
  render() {
    return (
      <div className="ui main text container">
        <Header />
        <Items className="my-items-class" items={ [1,2,3,4,5]}/>
      </div>
    )
  }
}

...
```

结果我们可以看
[src/component-step-component-props.js](src/component-step-component-props.js)
文件。

在上面的代码中，我们在 `Body` 组件中定义了，渲染 `Items` 组件，同时，给它设置了两个属性的值：
分别是：

- `className="my-items-class"` ：一个字符串
- `items={ [1,2,3,4,5]}` ：一个数组

#### 我们了解到了什么？

在 React 中，我们可以直接给组件设置属性，属性可以是字符串，也可以是数组，其实，我们可以传递任何
可以使用 JavaScript 表达的对象、数组、方法、变量等。

### 第三次拆分

有了上面的基础，我们也有了第三次拆分的大方向，

> 把 Items 拆分成父子关系的两个组件，父组件就是 `Items` ，而子组件就是 `Item`，同时，`Item`
> 组件不再是静态的输出了，而是我们会在 Items 中定义一个数组，然后遍历这个数组，每一个数组的元
> 都将被传入一个 `Item` 实例，然后渲染出这个 `Item`。

结果我们可以看 [src/component-step-3.js](src/component-step-3.js) 文件。

运行之后，得到了我们想要的结果，但是浏览器报了下面这个错误：

```bash
bundle.js:1293 Warning: Each child in an array or iterator should have a unique
"key" prop. Check the render method of `Items`. See
https://fb.me/react-warning-keys for more information.
    in Item (created by Items)
    in Items (created by Body)
    in div (created by Body)
    in Body (created by App)
    in div (created by App)
    in App
```

从上面这个错误信息可以看到，每一个从 `array` 或者 `iterator` 中遍历出来的子元素，都必须有一
个唯一的 `key` 属性，这个 `key` 是个什么鬼？

我们要知道当组件的属性发生了变化，其 `render` 方法会被重新调用，组件会被重新渲染。比如 `Items`
组件的中 `renderItems` 方法里面的 `items` 变量，如果不再是硬编码创建的数组，而是一个由其它地
方动态传递来，时常会更新的变量，那么，值的每一次变化，都会需要重新渲染一次 `Items`，由于其处在
一个长度不确定的数组中，React 需要判断，对数组中的每一项，到底是新建一个元素加入到页面中，
还是更新原来的元素。比如以下几种情况：

1. `[{ code: '1', name: 'A' }]` 变成 `[{ code: '1', name: 'B' }]` 这种情况明显只需要
  更新元素，没有必要重新创建元素；
2. `[{ code: '1'}]` 变成 `[{code: '1'}, {code: '2'}]` 这种情况明显就需要创建新元素了；
3. `[{ code: '1'}]` 变成 `[{ code: '2'}]`，这种情况就有点复杂了，似乎两种方案都可以。可以
  把表示 `code:1` 的元素删掉，为 `code:2` 新建一个，当然是非常合理的选择，但是直接把旧的元素
  换成新的，似乎也无不可。

实际上，如果真的认为上述第3种的后一种方案也无不可，那可是大错特错了。为什么呢，考虑一下面这种情况：

`[{code: '1'}, {code: '2'}]` 变成 `[{code: '2'}, {code: '1'}]`，我们总不能把元素都重新
修改一次， 把原来的 `code:1` 改成现在的 `code:2`，再把原来的 `code:2` 改成现在的`code:1`。

那么，为数组中的元素传一个唯一的 key（比如资产的唯一编码 `code`），就很好地解决了这个问题。
React 比较更新前后的元素 key 值，如果相同则更新，如果不同则销毁之前的，重新创建一个元素。

那么，为什么只有数组中的元素需要有唯一的 key，而其他的元素（比如上面菜单）则不需要呢？答案是：
React 有能力辨别出，更新前后元素的对应关系。这一点，也许直接看 JSX 不够明显，看 Babel 转换后
的 React.createElement 则清晰很多：

**转换前**

```es6
const Element = (
  <div>
    <h3>Example</h3>
    {[<p key={2}>hello</p>, <p key={1}>world</p>]}
  </div>
);
```

**转换后**

```javascript
"use strict";

var Element = React.createElement(
  "div",
  null,
  React.createElement(
    "h3",
    null,
    "example"
  ),
  [React.createElement(
    "p",
    { key: 2 },
    "hello"
  ), React.createElement(
    "p",
    { key: 1 },
    "world"
  )]
);
```

不管 `props` 如何变化，数组外的每个元素始终出现在 React.createElement() 参数列表中的固定
位置，这个位置就是天然的 key。

> 题外话：
> 初学 React 时还容易产生另一个困惑，那就是为什么 JSX 不支持 `if` 表达式来有选择地输出（不能
> 这样：`{if(yes){ <div {...props}/> }}）` ，而必须采用三元运算符来完成这项工作（必须这
> 样：`{yes ? <div {...props}/>} : null）`。那是因为，React 需要一个 `null` 去占住那个
> 元素本来的位置。

我们现在使用 `item.code` 作为 `key`，修复上面的这个问题，最终代码可以看：
[src/component-step-3-key-error-fixed.js](src/component-step-3-key-error-fixed.js)

### 第四次拆分

现在我们的 `Items` 组件中的数据是硬编码的，但是其实，它不应该被硬编码，`items` 数据有可能一般
是从服务器上面动态获取的，这个时候我们有两个选择：

1. 将数据放在 `Items` 组件的 `items` 属性中，然后将数据的获取放在其父亲元素中，在本项目里，也
  就是将数据的获取放在 `Body` 组件里；
2. 将数据获取放在 `Items` 组件中，但是由其 `props` 中的参数的不同来确定它将如何获取数据。

下面我们先看如何使用第一种方法如何实现，最终代码可以看：
[src/component-step-4.js](src/component-step-4.js)  文件。

对于第二种方法，我们在这里模拟了一个 `limit` 参数，用于通过父亲元素限定子元素展示的数据条数。
最终代码可以看 [src/component-step-4-2.js](src/component-step-4-2.js)  文件。修改
`Body` 中 `Items` 的 `limit` 值，可以看到界面上面会展示出不同条数的资产。

#### 那么，我该如何选择？

这是一个很麻烦的问题，但是可以从下面这两个方向去确定：

1. 如果 `Items` 模块只是一个通用的展示性组件，比如，它除了会展示资产条目外，还可以展示企业员工
   列表，那么，数据的获取就必须写在其父亲组件中；
2. 如果 `Items` 模块是一个具有独立功能的完整体，比如资产管理系统提供了一个叫作 `AssetItems`
   的组件，那么，数据的获取就必须应该写进组件内，它应该是一个完整的 *小应用*，甚至，它都应该自
   已解决对其它模块的依赖关系；

## 简单的交互

现在，我们已经完成了页面的渲染部分，接下来，再增加一个简单的交互，以完成本次课程，在页面列表的最
下方，添加一个按钮，当用户点击之后，加载更多资产数据，再添加另一个按扭，可以让用户刷新整个页面。

最终结果可以看文件：[src/interactive.js](src/interactive.js) 文件。

## 结语

我会开始，不会结束，下次见。
