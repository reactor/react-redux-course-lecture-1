import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class Navigator extends Component {
  render() {
    return (
      <div className="ui fixed inverted blue menu">
        <div className="ui container">
          <a href="#" className="item">我的资产</a>
          <a href="#" className="item">资产申请</a>
          <div className="right menu">
            <a href="#" className="header item">退出</a>
          </div>
        </div>
      </div>
    )
  }
}

class Header extends Component {
  render() {
    return (
      <h1 className="ui header">我的资产</h1>
    )
  }
}

class Item extends Component {
  constructor(props) {
    super(props);

    console.log(this.props);
  }

  render() {
    return (
      <div className="item">
        <div className="content">
          <a className="header">{this.props.name}</a>
          <div className="meta">
            <span>资产编码：{this.props.code}</span>
          </div>
          <div className="description">
            <p>备注：{this.props.note || '暂无'}</p>
          </div>
          <div className="extra">派发时间：{this.props.released}</div>
        </div>
      </div>
    )
  }

}

class Items extends Component {

  renderItems() {

    let items = this.props.items || [];
    return items.map(item => <Item name={item.name} code={item.code} note={item.note} released={item.released} key={item.code}/>)
  }

  render() {

    return (
      <div className="ui items">
        { this.renderItems() }
      </div>
    )
  }
}

class Body extends Component {
  getItems() {
    return [
      {
        name: '[MBP]Macbook Pro 15"',
        code: 'MBP201511250002',
        note: '该设备领取，右上角有划痕。',
        released: '2015年11月25日'
      }, {
        name: '[SCR]Dell 27" 2K 显示屏',
        code: 'MBP201511250003',
        note: '',
        released: '2015年11月25日'
      }, {
        name: '[SCR]VOC 27" 2K 显示屏',
        code: 'MBP201511250004',
        note: '',
        released: '2015年11月25日'
      }, {
        name: '[MOB]Apple iPhone 6s 128G',
        code: 'MBP201511250005',
        note: '该设备领取，右上角有划痕。',
        released: '2015年11月25日'
      }
    ];
  }
  render() {
    return (
      <div className="ui main text container">
        <Header/>
        <Items className="my-items-class" items={this.getItems()}/>
      </div>
    )
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <Navigator/>
        <Body/>
      </div>
    )
  }
}

export default() => {
  ReactDOM.render(
    <App/>, document.getElementById('root'));
}
