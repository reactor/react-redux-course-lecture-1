import React from 'react';
import ReactDOM from 'react-dom';

const App = () => {
  return (
    <div>
      <div className="ui fixed inverted blue menu">
        <div className="ui container">
          <a href="#" className="item">我的资产</a>
          <a href="#" className="item">资产申请</a>
          <div className="right menu">
            <a href="#" className="header item">退出</a>
          </div>
        </div>
      </div>

      <div className="ui main text container">
        <h1 className="ui header">我的资产</h1>
        <div className="ui items">
          <div className="item">
            <div className="content">
              <a className="header">{ '[MBP]Macbook Pro 15"'}</a>
              <div className="meta">
                <span>资产编码：MBP201511250002</span>
              </div>
              <div className="description">
                <p>备注：该设备领取，右上角有划痕。</p>
              </div>
              <div className="extra">派发时间：2015年11月25日</div>
            </div>
          </div>

          <div className="item">
            <div className="content">
              <a className="header">{ '[SCR]Dell 27" 2K 显示屏' }</a>
              <div className="meta">
                <span>资产编码：MBP201511250003</span>
              </div>
              <div className="description">
                <p>备注：暂无</p>
              </div>
              <div className="extra">派发时间：2015年11月25日</div>
            </div>
          </div>

          <div className="item">
            <div className="content">
              <a className="header">{ '[SCR]VOC 27" 2K 显示屏' }</a>
              <div className="meta">
                <span>资产编码：MBP201511250004</span>
              </div>
              <div className="description">
                <p>备注：暂无</p>
              </div>
              <div className="extra">派发时间：2015年11月25日</div>
            </div>
          </div>

          <div className="item">
            <div className="content">
              <a className="header">{ '[MOB]Apple iPhone 6s 128G' }</a>
              <div className="meta">
                <span>资产编码：MBP201511250005</span>
              </div>
              <div className="description">
                <p>备注：暂无</p>
              </div>
              <div className="extra">派发时间：2015年11月25日</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default () => {
  ReactDOM.render(<App />, document.getElementById('root'));
}
