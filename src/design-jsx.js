import React from 'react';
import ReactDOM from 'react-dom';

const app = () => {
  return (
    <div class="ui fixed inverted blue menu">
      <div class="ui container">
        <a href="#" class="item">我的资产</a>
        <a href="#" class="item">资产申请</a>
        <div class="right menu">
          <a href="#" class="header item">退出</a>
        </div>
      </div>
    </div>

    <div class="ui main text container">
      <h1 class="ui header">我的资产</h1>
      <div class="ui items">
        <div class="item">
          <div class="content">
            <a class="header">[MBP]Macbook Pro 15"</a>
            <div class="meta">
              <span>资产编码：MBP201511250002</span>
            </div>
            <div class="description">
              <p>备注：该设备领取，右上角有划痕。</p>
            </div>
            <div class="extra">派发时间：2015年11月25日</div>
          </div>
        </div>

        <div class="item">
          <div class="content">
            <a class="header">[SCR]Dell 27" 2K 显示屏</a>
            <div class="meta">
              <span>资产编码：MBP201511250003</span>
            </div>
            <div class="description">
              <p>备注：暂无</p>
            </div>
            <div class="extra">派发时间：2015年11月25日</div>
          </div>
        </div>

        <div class="item">
          <div class="content">
            <a class="header">[SCR]VOC 27" 2K 显示屏</a>
            <div class="meta">
              <span>资产编码：MBP201511250004</span>
            </div>
            <div class="description">
              <p>备注：暂无</p>
            </div>
            <div class="extra">派发时间：2015年11月25日</div>
          </div>
        </div>

        <div class="item">
          <div class="content">
            <a class="header">[MOB]Apple iPhone 6s 128G</a>
            <div class="meta">
              <span>资产编码：MBP201511250005</span>
            </div>
            <div class="description">
              <p>备注：暂无</p>
            </div>
            <div class="extra">派发时间：2015年11月25日</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default () => {
  ReactDOM.render(<app />, document.getElementById('root'));
}
