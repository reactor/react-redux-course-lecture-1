import React from 'react';
import ReactDOM from 'react-dom';

function Component() {
  return <h1>Hello, world!</h1>;
}

export default () => {
  ReactDOM.render(
    <Component />,
    document.getElementById('root')
  );
}
