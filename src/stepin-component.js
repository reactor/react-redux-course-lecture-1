import React from 'react';
import ReactDOM from 'react-dom';

function component() {
  return <h1>Hello, world!</h1>;
}

export default () => {
  ReactDOM.render(
    component,
    document.getElementById('root')
  );
}
